# EasyMow
Ce projet a été réalisé dans le cadre de la matière scala du Master 2 Logiciel UPEM 2019.

# Compiler le projet
Dans intelij, ouvrez le sbt shell et ecrivez la commande suivante :  compile

# Tester le projet
Dans intelij, ouvrez le sbt shell et ecrivez la commande suivante :  test

# Lancer le projet
Dans intelij, ouvrez le sbt shell et ecrivez la commande suivante :  run

# Fichier où écrire
Pour changer les mowers de position de départ ou de rajouter / enlever une commande, il faut modifier le fichier test.txt a la racine du projet.

# Choix d'implémentation
## Les Classes
### Position
Une classe simple qui définie une position comme un entier x et y.
### Command
Une classe qui définie les différentes commandes possible pour le mower.
Qui contient deux fonctions, une qui transforme un char en Command.
et une autre qui transforme un String en List de command
### Direction
Une classe qui définie la direction dans laquelle ce dirige le mower, avec la redéfinition de right et left qui détermine quelle direction se trouve a droite et a gauche pour chaque cas. Cela évite de faire le traitement à chaque fois.
Une fonction qui transforme un char en Direction et une autre une direction en char
### Mower
Une classe définie par un position, une direction et une position de limite de terrain.
Avec les fonction qui permette d'avancer de tourner, d'afficher le mower ...
### Main
Classe qui effectue le parsing et lance run sur le fichier "test.txt" à la racine du projet




