

case class Mower(position: Position, direction: Direction, lawn_limit: Position) {

  def doCommand(command : Command): Mower = {
    command match {
      case Command.Forward    => this.goForward()
      case Command.TurnLeft   => this.goLeft()
      case Command.TurnRight  => this.goRight()
      case Command.Undefined  => this
    }
  }

  def doListCommand(commands : List[Command], acc : Int) : Mower = {
    if (acc == commands.length-1){
      this.doCommand(commands(acc))
    }else {
      this.doCommand(commands(acc)).doListCommand(commands, acc + 1)
    }
  }

  def goForward(): Mower = {
    direction match {
      case Direction.North => if(canGoForward()) this.copy(new Position(position.x, position.y+1), direction, lawn_limit) else this
      case Direction.South => if(canGoForward()) this.copy(new Position(position.x, position.y-1), direction, lawn_limit) else this
      case Direction.West  => if(canGoForward()) this.copy(new Position(position.x-1, position.y), direction, lawn_limit) else this
      case Direction.East  => if(canGoForward()) this.copy(new Position(position.x+1, position.y), direction, lawn_limit) else this
    }
  }

  def canGoForward(): Boolean = {
    direction match {
      case Direction.North => if(lawn_limit.y == position.y) false else true
      case Direction.South => if(0 == position.y) false else true
      case Direction.West => if(0 == position.x) false else true
      case Direction.East => if(lawn_limit.x == position.x) false else true
    }
  }

  def goRight(): Mower = {
    this.copy(direction = direction.right)
  }

  def goLeft(): Mower = {
    this.copy(direction = direction.left)
  }

  def StringToPrint() : String  = {
    return this.position.x + " " + this.position.y + " " + Direction.directionToChar(this.direction)
  }


}


