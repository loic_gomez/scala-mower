

sealed trait Command

object Command {

  case object Forward extends Command
  case object TurnLeft extends Command
  case object TurnRight extends Command
  case object Undefined extends Command

  def CommandToDo(action : Char) : Command = {

    action match {
      case 'A' => Command.Forward
      case 'G' => Command.TurnLeft
      case 'D' => Command.TurnRight
      case _ => Command.Undefined
    }

  }

  def StringToCommandToDo(actions : String, acc : Int): List[Command] ={
    if (acc == actions.length){
      Nil
    }
    else{
      CommandToDo(actions(acc))::StringToCommandToDo(actions, acc+1)
    }
  }
}
