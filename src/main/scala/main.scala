import scala.io.Source

object Main extends App {

  val bufferedSource = Source.fromFile("test.txt")

  val fulltext = bufferedSource.mkString
  val lawn_x = fulltext(0).toInt
  val lawn_y = fulltext(2).toInt

  val mowers = fulltext.subSequence(5, fulltext.length).toString
  val lawn = new Position(lawn_x - 48,lawn_y - 48)
  if(lawn_x < 49 || lawn_x > 57 || lawn_y < 49 || lawn_y > 57){ // only limited to 1 to 9
    println("lawn value must be Integer !!")
    println("lawn value must be positive!!")
    println("lawn value must between 1 and 9 !!")
    println(fulltext(0) + " or " + fulltext(2) + " invalid !!")
    false
  }
  else{
    mowerDoAction(mowers, 0)
  }

  bufferedSource.close

  def parsing(commandtoextract: String, rmmowerend: Int ,acc: Int): String = {
    if (acc == rmmowerend){
      ""
    }else{
      commandtoextract(acc)+parsing(commandtoextract, rmmowerend ,acc+1)
    }
  }
  def mowerDoAction(text: String, acc: Int): Unit ={
    val splitToCheckEnd = text.split("\n")
    val occurrences = splitToCheckEnd.length - 1
    if (occurrences == 0){
      return Unit
    }
    if (acc == 0){

      val mowers = text

      if (verifDirection(mowers(4))) {// on verifie la validité de la direction
        val direction = Direction.charToDirection(mowers(4))

        if (verifPositionXY(mowers(0), mowers(2))) {// on verifie la validité du x, y de la position
          val mower = new Mower(new Position(mowers(0).toInt - 48, mowers(2).toInt - 48), direction, lawn)
          val rmmower = mowers.subSequence(mowers.indexOf("\n") + 1, mowers.length).toString
          val commands = parsing(rmmower, rmmower.indexOf("\n"), 0)
          println(mower.doListCommand(Command.StringToCommandToDo(commands, 0), 0).StringToPrint)
          mowerDoAction(rmmower, acc + 1)
        }
        else {
          return Unit
        }
      }
      else{
        return  Unit
      }
    }
    else {

      val mowers = text.subSequence(text.indexOf("\n") + 1, text.length).toString// on enleve la la ligne des commandes

      if (verifDirection(mowers(4))) { // on verifie la validité de la direction
        val direction = Direction.charToDirection(mowers(4))

        if (verifPositionXY(mowers(0), mowers(2))) {// on verifie la validité du x, y de la position
          val mower = new Mower(new Position(mowers(0).toInt - 48, mowers(2).toInt - 48), direction, lawn)
          val rmmower = mowers.subSequence(mowers.indexOf("\n") + 1, mowers.length).toString
          val commands = parsing(rmmower, rmmower.indexOf("\n"), 0)
          println(mower.doListCommand(Command.StringToCommandToDo(commands, 0), 0).StringToPrint)
          mowerDoAction(rmmower, acc + 1)
        }
        else {
          return Unit
        }
      }
      else{
        return Unit
      }
    }


  }

  def verifPositionXY(x: Char, y: Char): Boolean ={
    if(x.toInt < 48 || x.toInt > 57 || y.toInt < 48 || y.toInt > 57){ // only limited to 1 to 9
      println("position value must be Integer !!")
      println("position value must be positive!!")
      println("position value must between 0 and 9 !!")
      println(x + " or " + y + " invalid !!")
      false
    }
    else{
      true
    }
  }
  def verifDirection(direction: Char): Boolean  ={
    val directionmower = "[NEWS]{1}".r
    direction match {
      case directionmower() => true
      case _ => println("Direction must be N, E, S or W !!! value error : " + direction); false
    }
  }
}

/*
  val startreg = "[0-9] [0-9]".r
  val startmower = "[0-9] [0-9] [A-Z]".r
  val instructmower = "[A-Z]+".r
  val global = "[0-9] [0-9][\\r\\n][0-9] [0-9] [A-Z][\\r\\n][A-Z]+".r

  for (line <- bufferedSource.getLines) {
    println("-")
    line match {
      case global() => println("global")
      case startmower() => {
        val mower = new Mower(new Position(line(0).toInt - 48,line(2).toInt - 48), Direction.North, new Position(5,5));
        println(line); println("startmower")}
      case startreg() => val lawn = new Position(line(0).toInt - 48,line(2).toInt - 48);println(lawn.x, ' ', lawn.y); println(line); println("startreg")
      case instructmower() => println(line); println("instructmower")
      case _ => println(line.toUpperCase)
    }
}*/