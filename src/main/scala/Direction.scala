

sealed trait Direction {
  def left: Direction

  def right: Direction
}

object Direction {

  case object North extends Direction { // chaque direction redefinie le left and right, ce qui évite de devoir faire ce traitement dans le mower
    override def left = West            // avec un traitement beaucoup plus simple

    override def right = East
  }

  case object South extends Direction {
    override def left = East

    override def right = West
  }

  case object West extends Direction {
    override def left = South

    override def right = North
  }

  case object East extends Direction {
    override def left = North

    override def right = South
  }

  def charToDirection(direction: Char): Direction ={
    direction match {
      case 'N' => Direction.North
      case 'S' => Direction.South
      case 'E' => Direction.East
      case 'W' => Direction.West
    }
  }

  def directionToChar(direction: Direction): Char = {
    direction match {
      case North => 'N'
      case South => 'S'
      case West => 'W'
      case East => 'E'
    }
  }

}

