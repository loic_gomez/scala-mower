import org.scalatest.prop.GeneratorDrivenPropertyChecks
import org.scalatest._

class mowerTest extends FlatSpec with Matchers with GeneratorDrivenPropertyChecks {

  "forward" should "(0,1)" in {
    val mower = new Mower(new Position(0, 0), Direction.North, new Position(5, 5))
      .doCommand(Command.CommandToDo('A'))
    mower.position should be(new Position(0,1))
    mower.direction should be(Direction.North)
  }

  "rightThenForward" should "(1,0)" in {
    val mower = new Mower(new Position(0, 0), Direction.North, new Position(5, 5))
      .doCommand(Command.CommandToDo('D'))
      .doCommand(Command.CommandToDo('A'))
    mower.position should be(new Position(1,0))
    mower.direction should be(Direction.East)
  }

  "positionStartDiffRightThenForward" should "(1,0)" in {
    val mower = new Mower(new Position(4, 0), Direction.North, new Position(5, 5))
      .doCommand(Command.CommandToDo('G'))
      .doCommand(Command.CommandToDo('A'))
      .doCommand(Command.CommandToDo('A'))
      .doCommand(Command.CommandToDo('A'))
    mower.position should be(new Position(1,0))
    mower.direction should be(Direction.West)
  }

  "testLimitLawn" should "(0,5)" in {
    val mower = new Mower(new Position(0, 0), Direction.North, new Position(5, 5))
      .doCommand(Command.CommandToDo('A'))
      .doCommand(Command.CommandToDo('A'))
      .doCommand(Command.CommandToDo('A'))
      .doCommand(Command.CommandToDo('A'))
      .doCommand(Command.CommandToDo('A')) // 5
      .doCommand(Command.CommandToDo('A')) // ignore
      .doCommand(Command.CommandToDo('A')) // ignore
    mower.position should be(new Position(0,5))
    mower.direction should be(Direction.North)
  }

  "testLimitLawn" should "(0,0)" in {
    val mower = new Mower(new Position(0, 0), Direction.South, new Position(5, 5))
      .doCommand(Command.CommandToDo('A')) // ignore
      .doCommand(Command.CommandToDo('A')) // ignore
      .doCommand(Command.CommandToDo('A')) // ignore
      .doCommand(Command.CommandToDo('A')) // ignore
      .doCommand(Command.CommandToDo('A')) // ignore
      .doCommand(Command.CommandToDo('A')) // ignore
      .doCommand(Command.CommandToDo('A')) // ignore
    mower.position should be(new Position(0,0))
    mower.direction should be(Direction.South)
  }

  "testcharToDirection" should "(0,2)" in {
    val mower = new Mower(new Position(0, 0), Direction.charToDirection('N'), new Position(5, 5))
      .doCommand(Command.CommandToDo('A'))
      .doCommand(Command.CommandToDo('A'))
    mower.position should be(new Position(0,2))
    mower.direction should be(Direction.North)
  }

  "testPrintMower" should "2 3 E" in {
    val mower = new Mower(new Position(0, 0), Direction.charToDirection('N'), new Position(5, 5))
      .doCommand(Command.CommandToDo('A'))
      .doCommand(Command.CommandToDo('A'))
      .doCommand(Command.CommandToDo('A'))
      .doCommand(Command.CommandToDo('D'))
      .doCommand(Command.CommandToDo('A'))
      .doCommand(Command.CommandToDo('A'))
      .doCommand(Command.CommandToDo('A')) // extra to the East
      .doCommand(Command.CommandToDo('A')) // extra to the East
      .doCommand(Command.CommandToDo('G')) // 360 turn
      .doCommand(Command.CommandToDo('G'))
      .doCommand(Command.CommandToDo('A')) // cancel 1 extra to the East
      .doCommand(Command.CommandToDo('A')) // cancel 1 extra to the East
      .doCommand(Command.CommandToDo('D'))
      .doCommand(Command.CommandToDo('D')) // get the right direction
    mower.StringToPrint() should be("2 3 E")
    mower.direction should be(Direction.East)
    mower.position should be(new Position(2,3))
  }

  "testStringToCommand" should "2 3 E" in {
    val stringcommands = "AAADAAA TEST_ERROR AGGAADD"
    val mower = new Mower(new Position(0, 0), Direction.charToDirection('N'), new Position(5, 5))
      .doListCommand(Command.StringToCommandToDo(stringcommands, 0), 0)
    mower.StringToPrint() should be("2 3 E")
    mower.direction should be(Direction.East)
    mower.position should be(new Position(2,3))
  }


}
