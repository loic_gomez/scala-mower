lazy val `scala-mower` = (project in file(".")).
  settings(
    inThisBuild(List(
      scalaVersion := "2.12.7",
      version := "1.0.0",
    )),
    libraryDependencies += "org.scalatest" %% "scalatest" % "3.0.5" % "test",
    libraryDependencies += "org.scalacheck" %% "scalacheck" % "1.14.0" % "test",
  )